var user = {
    email: String,
    hashed_password: String,
    regDate: { type: Date, default: Date.now() }
};

try {
    module.exports = user;
} catch(err) {
    define([], function () {
        return user;
    });
}