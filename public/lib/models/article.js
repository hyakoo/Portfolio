var article = {
    title: String,
    author: String,
    contents: String,
    comments: [{body: String, nick: String, date: Date}],
    tags: {type: [String], index: true},
    hidden: Boolean,
    meta: {
        favs: Number
    },
    regDate: {type: Date, default: Date.now()}
};

try {
    module.exports = article;
} catch(err) {
    define([], function () {
        return article;
    });
}