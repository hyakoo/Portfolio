define(['loglevel', "Const", "app/layout/common/View", "app/service/ViewService"], function (log, Const, View, viewService) {
    'use strict';

    /*
     * private
     */
    var _this = null;
    var _navToggleRef = null;
    var _brandRef = null;
    var _introRef = null;
    var _boardRef = null;
    var _signinRef = null;

    /*
     * exports
     */
    function Header() {
        _this = this;
        this.clsName = "HEADER";
    };

    Header.prototype = new View(Const.HEADER_SELECTOR);

    Header.prototype.init = function () {
        log.info("[Header] init");

        _navToggleRef = _this.addDomRef(_this.clsName, _this.ref.find("div.navbar-header > button.navbar-toggle"));
        _brandRef = _this.addDomRef(_this.clsName, _this.ref.find("div.navbar-header > a.navbar-brand"));
        _introRef = _this.addDomRef(_this.clsName, _this.ref.find("ul.navbar-nav > li.intro"));
        _boardRef = _this.addDomRef(_this.clsName, _this.ref.find("ul.navbar-nav > li.board"));
        _signinRef = _this.addDomRef(_this.clsName, _this.ref.find("ul.navbar-nav > li > a.signin"));

        _this.bindEvent();
    };

    Header.prototype.bindEvent = function () {
        log.info("[Header] bindEvent");

        _.each(_this.domRefList(_this.clsName), function (ref) {
            if (ref.hasClass("nav-list")) {
                ref.click(function() {
                    if(!_navToggleRef.hasClass("collapsed")) {
                        _navToggleRef.trigger("click");
                    }
                });
            }
        });

        _brandRef.click(function () {
            location.href = "/";
        });

        _introRef.click(function () {
            $.ajax({
                type: "POST",
                url: "/intro",
                success: function (render) {
                    viewService.loadView(_this, render, "app/contents/Intro", null, {
                        css: ["app/contents/" + "intro"]
                    });
                },
                error: function (err) {
                    log.error(err.responseText);
                }
            });
        });

        _boardRef.click(function () {
            $.ajax({
                type: "POST",
                url: "/board",
                success: function (render) {
                    viewService.loadView(_this, render, "app/contents/Board", null, {
                        css: ["app/contents/" + "board"]
                    }, function(board) {
                        board.init();
                    });
                },
                error: function (err) {
                    log.error(err.responseText);
                }
            });
        });

        _signinRef.click(function () {
            $.ajax({
                type: "POST",
                url: "/main/signin",
                success: function (render) {
                    viewService.loadView(_this, render, "app/contents/modal/Signin", null, null, function (signin) {
                        signin.init();
                        signin.modal();
                    });
                },
                error: function (err) {
                    log.error(err.responseText);
                }
            });
        });
    };

    return Header;
});