define([ 'loglevel', "Const", "app/layout/common/View" ], function(log, Const, View) {
    'use strict';

    /*
     * private
     */
    var _this = null;

    /*
     * exports
     */
    function Footer(selector) {
        _this = this;
        this.clsName = "FOOTER";
    };

    Footer.prototype = new View(Const.FOOTER_SELECTOR);

    Footer.prototype.init = function() {
        log.info("[Footer] init");

        _this.bindEvent();
    };

    Footer.prototype.bindEvent = function() {
        log.info("[Footer] bindEvent");
    };

    return Footer;
});