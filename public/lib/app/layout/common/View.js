define(["loglevel", "Const", "jquery", "underscore"], function (log, Const) {
    'use strict';

    /*
     * private
     */
    var _this = null;
    var _clss = [];
    var _domRefs = {};
    var _auths = {};

    /*
     * exports
     */
    function View(parent) {
        _this = this;
        if (!_.isUndefined(parent)) {
            _this.parent = parent;
            _this.ref = $(_this.parent);
        } else {
            throw new Error("View parent 를 전달해주세요");
        }
    };

    View.prototype.init = function () {
        log.info("[View] init");
        log.warn("[View] Please implement this function");
    };

    View.prototype.findView = function (cls) {
        log.info("[View] findView");
        var _clsObj = null;
        _.each(_clss, function (clsObj) {
            if (_.isEqual(cls, clsObj)) {
                _clsObj = clsObj;
            }
        });

        return _clsObj;
    };

    View.prototype.addClass = function (cls) {
        try {
            if (!_.isUndefined(cls)) {
                if (_.isObject(cls)) {
                    _clss.push(cls);
                }
            } else {
                throw "인자가 부족합니다";
            }
        } catch (err) {
            log.error(err);
        }
    };

    View.prototype.addDomRef = function (clsName, domRef) {
        if (domRef.length > 0) {
            if(_.isEmpty(_domRefs[clsName])) {
                _domRefs[clsName] = [];
            }
            if(_.isArray(_domRefs[clsName])) {
                _domRefs[clsName].push(domRef);
            }
        } else {
            log.warn("유효하지 않은 DOM 참조입니다");
        }

        return domRef;
    };

    View.prototype.viewClsList = function () {
        return _clss;
    };

    View.prototype.domRefList = function (clsName) {
        if(!_.isUndefined(clsName)) {
            return _domRefs[clsName];
        } else {
            return _domRefs;
        }
    };

    View.prototype.authenticate = function(requiredAuth, callback) {
        // 구현 예정

        try {
            if(!_.isUndefined(callback)) {
                if(!_.isNull(callback) && _.isFunction(callback)) {
                    callback(true);
                }
            } else {
                throw "callback function이 필요합니다";
            }
        } catch(err) {
            log.error(err);
        }
    };

    View.prototype.destroy = function (clsName) {
        log.info("[View] destroy");
        if(!_.isUndefined(clsName)) {
            if(!_.isNull(clsName)) {
                _.each(_domRefs, function (refs, targetClsName) {
                    if (_.isEqual(clsName, targetClsName)) {
                        if(_.isArray(refs) && !_.isEmpty(refs)) {
                            _.each(refs, function(ref) {
                                ref.unbind();
                            });
                        }
                        delete _domRefs[targetClsName];
                    }
                });
                log.debug("[View] DomReference[" + clsName + "] 가 초기화 되었습니다");
            } else {
                log.debug("[View] 모든 DomReference 가 초기화 되었습니다");
            }
        }
    };

    return View;
});