define(['loglevel', "Const", "app/layout/common/View"], function (log, Const, View) {
    'use strict';

    /*
     * private
     */
    var _this = null;

    /*
     * exports
     */
    function Modal() {
        _this = this;
        this.clsName = "MODAL";
        _this.modalRef = $(_this.ref).parent();
    };

    Modal.prototype = new View(Const.MODAL_SELECTOR);

    Modal.prototype.modal = function () {
        log.info("[Modal] modal");
        log.error("[Modal] Please implement this function");
    };

    return Modal;
});