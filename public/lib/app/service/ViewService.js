/*
 * name: ViewService
 * desc: View를 화면에 표현하기위한 과정
 * exports: ViewService
 */
define(["loglevel", "app/util/validation"], function (log, validation) {
    'use strict';

    /*
     * private
     */
    var updateView = function (target, render, resource, callback) {
        log.info("[ViewService] updateView");
        try {
            validation.isSelector(target);
            validation.isRenderUpdate(target, render);
            validation.isResource(resource);
            validation.isCallback(callback);
        } catch (err) {
            log.error(err);
        }
    };

    /*
     * exports
     */
    var ViewService = {};

    ViewService.init = function () {
        log.info("[ViewService] init");
    };

    /*
     * name: loadView
     * desc: View 정보를 전달받아 화면을 로드
     * parameter
     *      view(object, not null): loadView 요청한 주체
     *      render(string, not null): rendering 된 html code
     *      clsPath(string, not null): 호출하는 View 의 경로
     *      res(string, nullable): 호출하는 View 의 resource 경로
     *      callback(function, nullable): View 호출 완료 후 추가적으로 필요한 작업
     */
    ViewService.loadView = function (view, render, clsPath, params, res, callback) {
        log.debug("[ViewService] loadView");
        require([clsPath], function (cls) {
            var _cls = null;
            if(!_.isUndefined(params)) {
                if(!_.isNull(params)) {
                    _cls = new cls(params);
                } else {
                    _cls = new cls();
                }
            } else {
                _cls = new cls();
            }
            var _loadedViewCls = view.findView(_cls);
            if (_.isNull(_loadedViewCls)) {
                log.debug("[ViewService] 새로운 View");
                view.addClass(_cls);
            } else {
                log.debug("[ViewService] 기존 View");
                _loadedViewCls.destroy(_loadedViewCls.clsName);

            }
            updateView(_cls.ref, render, res, function () {
                if (_.isFunction(callback)) {
                    callback(_cls);
                }
            });
        });
    };

    ViewService.addView = function (target, render, callback) {
        log.info("[ViewService] addView");
        try {
            validation.isSelector(target);
            validation.isRenderAppend(target, render);
            validation.isCallback(callback);
        } catch (err) {
            log.error(err);
        }
    };

    return ViewService;
});