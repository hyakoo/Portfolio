requirejs(["loglevel", "app/layout/basic/Header", "app/layout/basic/Footer", "underscore", "bootstrap"], function (log, Header, Footer) {
    'use strict';
    /*
     * ThirdParty 초기화
     */
    log.setLevel("debug");

    /*
     * layout 초기화
     */
    new Header().init();
    new Footer().init();
});