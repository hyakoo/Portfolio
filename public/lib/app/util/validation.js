/*
 * name: validation
 * desc: View 전환 시 유효성 검사를 진행하는 필터
 * exports: ViewService
 */
define([ "loglevel", "Const", "jquery", "underscore" ], function(log, Const) {
    'use strict';

    /*
     * private
     */

    /*
     * exports
     */
    var validation = {};

    validation.isSelector = function(selector) {
        if($(selector).length == 0) {
            throw "유효하지 않은 selector";
        } else if($(selector).length > 1) {
            throw "다중 selector";
        }
    };

    validation.isRenderUpdate = function(target, render) {
        if(!_.isNull(render)) {
            $(target).html(render);
        }
    };

    validation.isRenderAppend = function(target, render) {
        if(!_.isNull(render)) {
            $(target).append(render);
        }
    };

    validation.isCallback = function(callback) {
        if(!_.isUndefined(callback)) {
            if(!_.isFunction(callback)) {
                throw "유효하지 않은 타입";
            } else {
                callback();
            }
        }
    };

    validation.isResource = function(resource) {
        if(!_.isUndefined(resource) && _.isObject(resource)) {
            if(resource.hasOwnProperty("css")) {
                $(Const.RESOURCE_SELECTOR).find("link").remove();
                var _cssList = resource.css;
                if(_.isArray(_cssList)) {
                    _.each(_cssList, function(path) {
                        $('<link>').appendTo(Const.RESOURCE_SELECTOR)
                            .attr({type : 'text/css', rel : 'stylesheet'})
                            .attr('href', Const.RESOURCE_CSS_PATH + path + ".css");
                    });
                }
            }
            if(resource.hasOwnProperty("js")) {
                $(Const.RESOURCE_SELECTOR).find("script").remove();
                var _jsList = resource.js;
                if(_.isArray(_jsList)) {
                    _.each(_jsList, function(path) {
                        $('<script>').appendTo(Const.RESOURCE_SELECTOR)
                            .attr({type : 'text/javascript'})
                            .attr('src', Const.RESOURCE_JS_PATH + path + ".js");
                    });
                }
            }
        } else if (_.isNull(resource)) {
            //$(Const.RESOURCE_SELECTOR).find("link").remove();
        } else {
            throw "지원하지 않는 타입";
        }
    };

    return validation;
});