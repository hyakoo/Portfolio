define(['loglevel', "Const", "app/layout/common/Modal", "app/service/ViewService"], function (log, Const, Modal, viewService) {
    'use strict';

    /*
     * private
     */
    var _this = null;
    var _signupRef = null;

    /*
     * exports
     */
    function Signin() {
        _this = this;
        this.clsName = "SIGNIN";
    };

    Signin.prototype = new Modal();

    Signin.prototype.init = function (render) {
        log.info("[Signin] init");
        _signupRef = _this.ref.find("div.modal-footer > #signup");
        _this.bindEvent();
    };

    Signin.prototype.bindEvent = function () {
        log.info("[Signin] bindEvent");
        var _this = this;

        _signupRef.click(function () {
            $.ajax({
                type: "POST",
                url: "/main/signup",
                success: function (render) {
                    viewService.loadView(_this, render, "app/contents/modal/Signup", null, null, function(signup) {
                        signup.init();
                    });
                },
                error: function (err) {
                    log.error(err.responseText);
                }
            });
        });
    };

    Signin.prototype.modal = function () {
        log.info("[Signin] modal");
        $(_this.modalRef).modal();
    };

    return Signin;
});