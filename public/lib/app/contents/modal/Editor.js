define(['loglevel', "Const", "app/layout/common/Modal", "app/service/ViewService", "ckeditor", "models/article"], function (log, Const, Modal, viewService, ckeditor, article) {
    'use strict';

    /*
     * private
     */
    var _this = null;
    var _titleInputRef = null;
    var _saveButtonRef = null;
    var _delButtonRef = null;

    /*
     * exports
     */
    function Editor(mode) {
        _this = this;
        this.clsName = "EDITOR";
        if(!_.isUndefined(mode)) {
            this.mode = mode;
        } else {
            throw new Error("Editor Mode를 지정해주세요");
        }
    };

    Editor.prototype = new Modal();

    Editor.prototype.init = function (render) {
        log.info("[Editor] init");

        ckeditor.replace("editor");

        _titleInputRef = _this.addDomRef(_this.clsName, _this.ref.find("input#article_title"));
        _saveButtonRef = _this.addDomRef(_this.clsName, _this.ref.find("div.modal-footer > #save"));
        _delButtonRef = _this.addDomRef(_this.clsName, _this.ref.find("div.modal-footer > #del"));

        _this.authenticate({
            clsName: _this.clsName,
            auType: "delete"
        }, function(authFlag) {
            if(!authFlag && _.isEqual(_this.mode, "add")) {
                _delButtonRef.hide();
            }
        });

        _this.bindEvent();
    };

    Editor.prototype.bindEvent = function () {
        log.info("[Editor] bindEvent");
        var _this = this;

        _saveButtonRef.click(function() {
            article.title = _titleInputRef.val();
            article.author = "hyakoo";
            article.contents = ckeditor.instances.editor.getData().trim();

            $.ajax({
                type: "POST",
                url: "/board/save",
                data: article,
                success: function (result) {
                    alert("등록을 완료했습니다");
                    $(_this).triggerHandler("updateData");
                    _this.modal("hide");
                },
                error: function (err) {
                    alert("등록을 실패했습니다");
                    if(err.responseJSON) {
                        var _error = err.responseJSON[0];
                        log.error(_error.name + ": " + _error.message);
                    }
                }
            });
        });

    };

    Editor.prototype.modal = function (action) {
        log.info("[Editor] modal");

        //Event Bind: modal is loaded
        $(_this.modalRef).off("shown.bs.modal");
        $(_this.modalRef).on("shown.bs.modal", function() {
            _titleInputRef.focus();
        });

        var _action = _.isUndefined(action) ? "show" : action;
        $(_this.modalRef).modal(_action);
    };

    return Editor;
});