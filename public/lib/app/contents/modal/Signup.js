define(['loglevel', "app/layout/common/Modal" , "app/service/ViewService"], function (log, Modal, viewService) {
    'use strict';

    /*
     * private
     */
    var _this = null;

    /*
     * exports
     */
    function Signup() {
        _this = this;
        this.clsName = "SIGNUP";
    };

    Signup.prototype = new Modal();

    Signup.prototype.init = function () {
        log.info("[Signup] init");
        _this.bindEvent();
    };

    Signup.prototype.bindEvent = function () {
        log.info("[Signup] bindEvent");
        var _this = this;
    };

    Signup.prototype.modal = function() {
        log.info("[Signup] modal");
        $(_this.ref).modal();
    };

    return Signup;
});