define(["loglevel", "Const", "app/layout/common/View", "app/service/ViewService", "text!data_partial/board/article.html"], function (log, Const, View, viewService, articleHtml) {
    'use strict';

    /*
     * private
     */
    var _this = null;
    var _articleListDataRef = null;
    var _writeButtonRef = null;

    /*
     * exports
     */
    function Board() {
        _this = this;
        this.clsName = "BOARD";
    };

    Board.prototype = new View(Const.CONTENTS_SELECTOR);

    Board.prototype.init = function () {
        log.info("[Board] init");

        _articleListDataRef = _this.addDomRef(_this.clsName, _this.ref.find("div > table#article_list > tbody"));
        _writeButtonRef = _this.addDomRef(_this.clsName, _this.ref.find("div.func_icon > i.material-icons"));

        _this.bindEvent();
        _this.selectArticleList();
    };

    Board.prototype.bindEvent = function () {
        log.info("[Board] bindEvent");

        _writeButtonRef.click(function() {
            $.ajax({
                type: "POST",
                url: "/board/write",
                success: function (render) {
                    viewService.loadView(_this, render, "app/contents/modal/Editor", "ADD", null, function (editor) {
                        editor.init();
                        editor.modal();

                        $(editor).unbind("updateData");
                        $(editor).bind("updateData", function() {
                            _this.selectArticleList();
                        });

                    });
                },
                error: function (err) {
                    log.error(err.responseText);
                }
            });
        });
    };

    Board.prototype.selectArticleList = function(where) {
        $.ajax({
            type: "POST",
            url: "/board/list",
            success: function (data) {
                var _articleHtmlRef = $(articleHtml);
                _articleListDataRef.html("");
                if(data.length == 0) {
                    _articleHtmlRef.find("td:first").prop("colspan", 3).css("text-align", "center").html("등록된 게시물이 존재하지 않습니다");
                    _articleListDataRef.append(_articleHtmlRef);
                } else {
                    _.each(data, function(article) {
                        var _tempArticleHtmlRef = _articleHtmlRef.clone();
                        _tempArticleHtmlRef.find("td.grid_title").text(article.title);
                        _tempArticleHtmlRef.find("td.grid_author").text(article.author);
                        _tempArticleHtmlRef.find("td.grid_regDate").text(new Date(article.regDate).format("yy.MM.dd") + "\n" + new Date(article.regDate).format("HH:mm:ss"));
                        _articleListDataRef.append(_tempArticleHtmlRef);
                    });
                }
            },
            error: function (err) {
                log.error(err.responseText);
            }
        });
    };

    return Board;
});