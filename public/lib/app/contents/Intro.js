define(["loglevel", "Const", "app/layout/common/View"], function (log, Const, View) {
    'use strict';

    /*
     * private
     */
    var _this = null;

    /*
     * exports
     */
    function Intro() {
        _this = this;
        this.clsName = "INTRO";
    };

    Intro.prototype = new View(Const.CONTENTS_SELECTOR);

    Intro.prototype.init = function () {
        log.info("[Intro] init");
        _this.bindEvent();
    };

    Intro.prototype.bindEvent = function () {
        log.info("[Intro] bindEvent");

    };

    return Intro;
});