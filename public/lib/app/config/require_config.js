requirejs.config({
    baseUrl: "lib",
    paths: {
        "jquery": "vender/jquery/jquery.min",
        "bootstrap": "vender/bootstrap/js/bootstrap.min",
        "underscore": "vender/underscore.min",
        "text": "vender/requirejs/plugin/text",
        "stylesheets": "../stylesheets",
        "loglevel": "vender/loglevel",
        "Const": "app/config/app_config",
        "ckeditor": "vender/ckeditor/ckeditor",
        "models": "models"
    },
    shim: {
        "bootstrap": {
            deps: ["jquery"]
        },
        "ckeditor": {
            exports: "CKEDITOR"
        }
    }
});