define([], function() {
    'use strict';

    var Config = {
        ROOT_CONTEXT_PATH: "/",
        RESOURCE_SELECTOR: "section.customResource",
        RESOURCE_JS_PATH: "/lib/",
        RESOURCE_CSS_PATH: "/stylesheets/",
        HEADER_SELECTOR: "section.header_section",
        CONTENTS_SELECTOR: "section.contents_section",
        FOOTER_SELECTOR: "section.footer_section",
        MODAL_SELECTOR: "section.modal_section > .modal > .modal-dialog"
    };

    return Config;
});