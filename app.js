var express = require("express");
var engine = require("ejs-locals");
var session = require("express-session");
var path = require("path");
var favicon = require("serve-favicon");
var _ = require("underscore");
var logger = require("morgan");
var cookieParser = require("cookie-parser");
var bodyParser = require("body-parser");
var mongoose = require("mongoose");
var config = require("config");
var favicon = require("serve-favicon");

var app = express();

// view engine setup
app.engine("ejs", engine);
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

app.use(session({
    secret: "keyboard cat",
    resave: false,
    saveUninitialized: true
}));

app.use(favicon(path.join(__dirname, "public", "images", "common", "favicon.ico")));
app.use(logger("dev"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

require("./config/passport")(app);
require("./config/route")(app);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error("Not Found");
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get("env") === "development") {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render("error", {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render("error", {
        message: err.message,
        error: {}
    });
});

mongoose.connect(config.get("db.host"), function (err) {
    if (err) {
        console.log("mongoose connection error :" + err);
        throw err;
    }

    app.listen(config.get("http.port"), function (req, res) {
        console.log("server start...");
    });
});