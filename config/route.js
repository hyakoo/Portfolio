/*
 * Module dependencies
 */

var express = require("express");
var router = express.Router();

/*
 * Expose
 */

module.exports = function (app) {
    var home = require("../controllers/home");
    var login = require("../controllers/login");
    var main = require("../controllers/main");
    var intro = require("../controllers/intro");
    var board = require("../controllers/board");

    app.use("/", home);
    app.use("/auth", login);
    app.use("/main", main);
    app.use("/intro", intro);
    app.use("/board", board);

}