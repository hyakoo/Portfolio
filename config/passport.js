/*
 * Module dependencies
 */
var express = require("express");
var mongoose = require("mongoose");
var passport = require("passport");

var LocalStrategy = require("passport-local").Strategy;

/*
 * Expose
 */

module.exports = function(app) {
    app.use(passport.initialize());
    app.use(passport.session());

    passport.serializeUser(function(user, done) {
        console.log("serializeUser");
        done(null, user);
    });

    passport.deserializeUser(function(user, done) {
        console.log("deserializeUser");
        done(null, user);
    });

    passport.use(new LocalStrategy({
                usernameField: "email",
                passwordField: "password",
                passReqToCallback : true
            },
            function(req, email, password, done) {
                var userModel = require("../models/user")(mongoose);
                userModel.findOne({email: email}).exec(function(err, user) {
                    if (err) {
                        return done(err);
                    }
                    if (!user) {
                        return done(null, false, { message: "Unkown user" });
                    }
                    if (!user.authenticate(password)) {
                        return done(null, false, { message: "Invalid password" })
                    }
                    return done(null, user);
                });
            }
        )
    );
}