/*
 * Module dependencies
 */
var express = require("express");
var mongoose = require("mongoose");
var passport = require("passport");

var router = express.Router();

router.route("/login")
    .all(function(req, res, next) {
        console.log("login");
        next();
    })
    .post(passport.authenticate("local", {successRedirect: "/main", failureRedirect: "/auth/login_fail"}), function (req, res) {

    });

router.route("/login_fail")
    .all(function(req, res, next) {
        console.log("fail");
        next();
    })
    .get(function (req, res) {
        res.render("index", {
            locals: {
                title: "Hyakoo's Blog",
                message: "정확하지 않은 로그인 정보"
            }
        });
    });

/*
 * Expose
 */

module.exports = router;