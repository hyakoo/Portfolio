/*
 * Module dependencies
 */
var express = require("express");
var mongoose = require("mongoose");
var passport = require("passport");

var router = express.Router();

router.route("/")
    .all(function(req, res, next) {
        console.log("main");
        if(req.isAuthenticated()) {
            next();
        } else {
            res.redirect("/");
        }
    })
    .get(function (req, res, next) {
        res.redirect("/");
    });

router.route("/signin")
    .all(function(req, res, next) {
        console.log("signin");
        next();
    })
    .post(function (req, res, next) {
        res.render("app/partial/modal/signin", {});
    });

router.route("/signup")
    .all(function(req, res, next) {
        console.log("signup");
        next();
    })
    .post(function (req, res, next) {
        res.render("app/partial/modal/signup", {});
    });

/*
 * Expose
 */

module.exports = router;