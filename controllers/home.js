/*
 * Module dependencies
 */
var express = require("express");
var mongoose = require("mongoose");
var passport = require("passport");

var router = express.Router();

router.route("/")
    .all(function(req, res, next) {
        console.log("home");
        next();
    })
    .get(function (req, res, next) {
        if(req.isAuthenticated()) {
            res.redirect("/main");
        } else {
            res.render("index", {
                title: "Hyakoo\'s Blog"
            });
        }
    });

router.route("/signup")
    .all(function(req, res, next) {
        console.log("signup");
        next();
    })
    .get(function (req, res) {
        res.render("signup", {
            title: "Hyakoo's Blog"
        });
    });

router.route("/signup_success")
    .all(function(req, res, next) {
        console.log("signup_success");
        next();
    })
    .post(function (req, res) {
        var userModel = require("../models/user")(mongoose);
        new userModel({
            email: req.body.email,
            password: req.body.password
        }).save(function(err, user) {
                if (err) {
                    throw err;
                }
                res.redirect("/");
            });
    });

router.route("/logout")
    .all(function(req, res, next) {
        console.log("logout...");
        next();
    })
    .get(function(req, res) {
        console.log("로그인페이지로 이동합니다");
        req.logout();
        res.redirect("/");
    });

/*
 * Expose
 */

module.exports = router;