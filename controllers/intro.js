/*
 * Module dependencies
 */
var express = require("express");
var mongoose = require("mongoose");
var passport = require("passport");

var router = express.Router();

router.route("/")
    .post(function (req, res, next) {
        res.render("app/partial/intro/intro", {
            title: "INTRODUCE"
        });
    });

/*
 * Expose
 */

module.exports = router;