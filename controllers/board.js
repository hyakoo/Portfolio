/*
 * Module dependencies
 */
var express = require("express");
var logger = require("morgan");
var _ = require("underscore");
var mongoose = require("mongoose");
var passport = require("passport");

var router = express.Router();

router.route("/")
    .post(function (req, res, next) {
        res.render("app/partial/board/board", {
            title: "BOARD"
        });
    });

router.route("/list")
    .post(function (req, res, next) {
        var articleModel = require("../models/article")(mongoose);
        articleModel.find(function(err, docs) {
            res.json(docs);
        }).sort({regDate: -1});
    });

router.route("/write")
    .post(function (req, res, next) {
        res.render("app/partial/modal/editor", {
            title: "BOARD",
            articleTitle: ""
        });
    });

router.route("/save")
    .post(function (req, res, next) {
        var articleModel = require("../models/article")(mongoose);
        new articleModel({
            title: req.body.title,
            author: req.body.author,
            contents: req.body.contents
        }).save(function(err, article) {
            if (err) {
                var _errors = [];
                _.each(err.errors, function(err, name) {
                    _errors.push({
                        name: err.name,
                        message: err.message
                    });
                });
                res.status(500).send(_errors);
            }
            res.json(article);
        });
    });

/*
 * Expose
 */

module.exports = router;