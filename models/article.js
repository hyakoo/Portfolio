/*
 * Module dependencies
 */

var _ = require("underscore");
var article = require("../public/lib/models/article");

/*
 * Expose
 */

module.exports = function(db) {
    try {
        return db.model('Article');
    } catch (e) {
    }

    var Schema = db.Schema;

    var ArticleSchema = new Schema(article);

    /**
     * Virtuals
     */


    /**
     * Validations
     */

    var validatePresenceOf = function (value) {
        if(_.isNull(value) || _.isEmpty(value)) {
            return false;
        } else {
            return true;
        }
    };

    ArticleSchema.path("title").validate(validatePresenceOf, "Title cannot be blank");
    ArticleSchema.path("author").validate(validatePresenceOf, "Author cannot be blank");
    ArticleSchema.path("contents").validate(validatePresenceOf, "Contents cannot be blank");

    /**
     * Methods
     */

    ArticleSchema.methods = {};


    return db.model('Article', ArticleSchema);
};

//module.exports.model = new function() {
//    this.title = null;
//    this.author = null;
//    this.contents = null;
//    this.comments = [{contents: null, nick: null, date: null}];
//    this.tags = [];
//    this.hidden = null;
//    this.meta = {
//        favs: null
//    };
//    this.regDate = null;
//};