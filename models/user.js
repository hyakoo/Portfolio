/*
 * Module dependencies
 */

var crypto = require("crypto");
var user = require("../public/lib/models/user");

/*
 * Expose
 */

module.exports = function(db) {
    try {
        return db.model('User');
    } catch (e) {}


    var Schema = db.Schema;

    var UserSchema = new Schema(user);

    /**
     * Virtuals
     */

    UserSchema
        .virtual("password")
        .set(function(password) {
            this.hashed_password = this.encryptPassword(password);
        })
        .get(function() {
            return this.password;
        });

    /**
     * Validations
     */

    var validatePresenceOf = function(value) {
        return value && value.length;
    };

    UserSchema.path("email").validate(validatePresenceOf, "Email cannot be blank");
    UserSchema.path("hashed_password").validate(validatePresenceOf, "Password cannot be blank");

    /**
     * Methods
     */

    UserSchema.methods = {
        /**
         * Authenticate - check if the passwords are the same
         *
         * @param {String} plainText
         * @return {Boolean}
         * @api public
         */

        authenticate: function(password) {
            return this.hashed_password === this.encryptPassword(password);
        },

        /**
         * Encrypt password
         *
         * @param {String} password
         * @return {String}
         * @api public
         */
        encryptPassword: function(password) {
            if (!password) {
                return "";
            }

            try {
                var shasum = crypto.createHash("sha1");
                return shasum.update(password).digest("hex");
            } catch (err) {
                return "";
            }
        }
    }


    return db.model('User', UserSchema);
};